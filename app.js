'use strict';

const express = require('express');
const app = express();
const logger = require('morgan');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(logger('dev')); // logger

// Application ROUTES
let mainRoutes = require('./routes');
let transactionsRoutes = require('./routes/transactions_routes');
let balancesRoutes = require('./routes/balances_routes');

app.use(mainRoutes);
app.use('/transactions', transactionsRoutes);
app.use('/balances', balancesRoutes);

app.listen(3000, () => {
  console.log('Yalla namshi');
});

// catch 404 and forward to error handler
app.use(function(req, res, next){
	var err = new Error("Not Found");
	err.status = 404;
	next(err);
});

// Error handler
app.use(function(err, req, res, next){
	res.status(err.status || 500);
	res.json({
		error: {
			message: err.message
		}
	});
});
