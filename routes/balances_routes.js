const express = require('express');
let router = express.Router();
let Balance = require('../model/balances');

router.post('/', async (req, res) => {
    try {
        var account = await Balance.create({
            account_nr: req.body.account_nr,
            balance: req.body.balance
        });

        res.status(200).json({
            success: { message: "account has been successfully created!"}
        });
    } catch(err) {
        res.status(404).json({
            error: {
                message: `${req.body.account_nr} account creation failed!, ${err.message}`
            }
        });
    }
});

router.get('/:account', async (req, res) => {
    try {
        var { account } = req.params;
        var result = await Balance.findOne({ where: {account_nr: account}});
        res.status(200).json(result.dataValues);
    } catch(err) {
        res.status(404).json({
            error: {
                message: `${account} account does not exist!`
            }
        });
    }
});

module.exports = router;
