const express = require('express');
let router = express.Router();

router.get('/', (req, res) => {
    res.status(200).json(
    {
        "name":"Namshi Demo",
        "version":"1.0",
        "contact":{"email":"omaralkhairat@gmail.com"}
    });
});

module.exports = router;
