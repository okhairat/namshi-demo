const express = require('express');
const router = express.Router();
const validator = require('validator');
let { connection } = require('../model/mysql');
let Transaction = require('../model/transactions');
let Balance = require('../model/balances');

async function transfer(from, to, amount, sqlTransaction) {
    return await Transaction.bulkCreate([
        {amount: -amount, account_nr: from},
        {amount: +amount, account_nr: to}
    ], { validate: true, transaction: sqlTransaction });
}

async function getAccountWithUpdateLock(account, sqlTransaction) {
    return await Balance.findOne({
        where: {account_nr: account},
        transaction: sqlTransaction,
        lock: sqlTransaction.LOCK.UPDATE
    }, {transaction: sqlTransaction});
}

async function withdraw(from, amount, sqlTransaction) {
    // withdraw amount from origin account
    let account = await getAccountWithUpdateLock(from, sqlTransaction);
    if(!account){
        throw new Error(`${from} account does not exist!`);
    }
    if(account.balance < amount) {
        throw new Error(`${from} account's balance is insufficient!`);
    }
    var newBalance = account.balance - parseFloat(amount);
    return await account.update(
        { balance: newBalance},
        { transaction: sqlTransaction }
    );
}

async function deposit(to, amount, sqlTransaction) {
    // deposit amount to destination account
    let account = await getAccountWithUpdateLock(to, sqlTransaction);
    if(!account){
        throw new Error(`${to} account does not exist!`);
    }

    var newBalance = account.balance + parseFloat(amount);
    return await account.update(
        { balance: newBalance},
        { transaction: sqlTransaction }
    );
}

// validate accounts input middleware
router.post('/', (req, res, next) => {
    if(!req.body.from || !req.body.to) {
        var err = new Error('Bad request, please make sure {from} and {to} accounts are defined');
        err.status = 400;
        next(err);
    }
    if(req.body.from === req.body.to) {
        var err = new Error('transaction at same account is not allowed!');
        err.status = 400;
        next(err);
    }
    next();
});

// validate money amount middleware
router.post('/', (req, res, next) => {
    if(!req.body.amount) {
        var err = new Error('Bad request, please make sure {amount} is defined');
        err.status = 400;
        next(err);
    }

    if(!validator.isCurrency(req.body.amount.toString(), {allow_negatives: false})) {
        var err = new Error('amount is not a valid currency!');
        err.status = 400;
        next(err);
    }
    next();
});

router.post('/', (req, res) => {
    let amount = req.body.amount;

    return connection.transaction().then(async function (t) {
        try {
            // chain all your queries here. make sure you return them.
            let result = await Promise.all([
                withdraw(req.body.from, req.body.amount, t),
                deposit(req.body.to, req.body.amount, t),
                transfer(req.body.from, req.body.to, req.body.amount, t)
            ]);

            await t.commit();

            // Transaction has been committed
            // result is whatever the result of the promise chain returned to the transaction callback
            res.status(200).json({
        		success: {
        			message: "transaction has been successfully made!"
        		}
        	});

        } catch(err) {
            await t.rollback();

            // Transaction has been rolled back
            // err is whatever rejected the promise chain returned to the transaction callback
            res.status(404).json({
        		error: {
        			message: err.message
        		}
        	});
        }
    });
});

router.get('/', (req, res) => {
    return Transaction.findAll({ raw: true }).then(result => {
        res.status(200).json(result);
    }).catch(err =>{
        res.status(404).json({
    		error: {
    			message: err.message
    		}
        });
    });
});

module.exports = router;
