FROM mhart/alpine-node:8

RUN npm install -g yarn && yarn global add nodemon && yarn

COPY . /src
WORKDIR /src

EXPOSE 3000

CMD ["node", "app.js"]
