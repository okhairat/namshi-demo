# README #

This document the steps that are necessary to get namshi-demo application up and running.

### Application Description ###
basically it is an API application that handles bank account transactions between two
accounts, the API has two endpoints (`/transactions`) and (`/balances`).
you can use (`/balances`) endpoint to create accounts with balances, the request body
would look like (`{account_nr: account_nr, balance: balance}`).
while (`/transactions`) endpoint is for registering account transactions by posting
body params like (`{from: account, to: account, amount: money}`)

### What are the tools used for this project? ###

* Docker with docker-composer
* Node.js with Express framework
* MySql as a backend database
* Sequelize a promise-based ORM for Node.js v4

### How do I get set up? ###

* on root directory run `npm install` to install dependencies
* run `docker-composer build` and `docker-composer up`
* configure mysql.js and make `host` to point to docker ip, ex: `host: '192.168.99.100'`
* Test application and have fun :)
