const Sequelize = require('sequelize');
let connection = new Sequelize('namshi', 'namshi', 'namshi', {
    host: '192.168.99.100',
    dialect: "mysql",
    port: 3306,
    pool: {
        // Set maxIdleTime to 20 seconds. Otherwise, it kills transactions that
        // are open for more than 20 seconds.
        maxIdleTime: 20000,
    },
});

connection.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    next(err);
  });

module.exports = {Sequelize, connection};
