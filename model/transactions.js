let Sequelize = require('./mysql').Sequelize;
let connection = require('./mysql').connection;

// the money value will have only 2 digits after the decimal so the values
// that comes after the decimal will be rounded
function toCurrency(number) {
    return parseFloat(number).toFixed(2).replace(/(\d)(?=(\d{*})+\.)/g);
}

let Transaction = connection.define('transaction', {
    reference: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    amount: {
        type: Sequelize.DECIMAL(10, 2),
        defaultValue: 0.00,
        allowNull: false,
        validate: {
            isDecimal: { msg: "Must be a decimal without comma ','"}
        },
        get() {
            return parseFloat(this.getDataValue('amount'));
        },
        set(val) {
            this.setDataValue('amount', toCurrency(val));
        }
    },
    account_nr: {
        type: Sequelize.STRING,
        allowNull: false
    }
  },{
    timestamps: false
});

connection.sync({logging: console.log});
module.exports = Transaction;
