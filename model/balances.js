const Sequelize = require('./mysql').Sequelize;
let connection = require('./mysql').connection;

// the money value will have only 2 digits after the decimal so the values
// that comes after the decimal will be rounded
function toCurrency(number) {
    return parseFloat(number).toFixed(2).replace(/(\d)(?=(\d{*})+\.)/g);
}

const Balance = connection.define('balance', {
    account_nr: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        primaryKey: true
    },
    balance: {
        type: Sequelize.DECIMAL(10, 2),
        defaultValue: 0.00,
        allowNull: false,
        validate: {
            isDecimal: { msg: "Balance must be a decimal format without comma ','"}
        },
        get() {
            return parseFloat(this.getDataValue('balance'));
        },
        set(val) {
            this.setDataValue('balance', toCurrency(val));
        }
    }
},{
  timestamps: false
});

connection.sync({logging: console.log});

module.exports = Balance;
